

# Terms and Conditions of Use

By visiting the Kaiko website, and utilizing the data, information, tools, APIs, services and products ("Resources") available here, you demonstrate that you understand, accept and agree to adhere to Kaiko's terms and conditions ("T&C") as stated here.

Kaiko (“The Company”) reserves the right to change the T&C periodically without notice. Should you wish to continue using the Resources in future, you acknowledge and agree that you have a responsibility to review the terms on a regular in order to be aware of any changes.

You also agree to use the Resources only for the purposes allowed by the T&C and any laws or regulations in France, the European Union and your local jurisdiction. 

Resources may be accessed only by the means provided by Kaiko. Access, attempted or successful, of the Resources here via other means is not permitted. Access or attempted access, interference with or disruption to the networks or servers where the Resources are located is prohibited.

Kaiko does not permit the resale, copying, reproduction or duplication of any Resources provided here, with or without source attribution.

For terms related to the consequences of using Kaiko data, please see our Data Use Terms and Disclaimer [here](LINK).
